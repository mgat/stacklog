import React, { Component } from 'react';
import Backlog from "./Backlog";


class App extends Component {
  render(){
    return(
        <div className="App">
          <Backlog/>
        </div>
    )
  }
}

export default App;
