import React from "react";
import styled from "styled-components";
import {Droppable} from "react-beautiful-dnd";
import Todo from "./Todo"

const Container = styled.div`
    margin: 0px;
    border-radius: 2px;
    width: 100%;
    display:flex;
    flex-direction: column;
    
`;
const Title = styled.h3`
    padding: 8px;
    text-align: center;
`;
const TodoList = styled.div`
    padding: 8px;
    background-color: ${props => (props.isDraggingOver ? 'skyblue' : 'white')};
    flex-grow:1;
    min-height: 10px;
    min-width: 100px;
`;


export default class Column extends React.Component{
    render() {
        return(
            <Container>
                <Title>
                    {this.props.column.title}
                </Title>
                <Droppable droppableId={this.props.column.id}>
                    {(provided,snapshot) => (
                        <TodoList
                            ref={provided.innerRef}
                            className={this.props.column.id}
                            {...provided.droppableProps}
                            isDraggingOver={snapshot.isDraggingOver}
                        >
                            {this.props.todos.map((todo,index) => (
                                <Todo key={todo.id} todo={todo} index={index}/>
                            ))}
                            {provided.placeholder}
                        </TodoList>
                    )}
                </Droppable>
            </Container>
        );
    }

}