import React from 'react';
import ReactDOM from 'react-dom';
import '@atlaskit/css-reset';
import {DragDropContext} from "react-beautiful-dnd";
import * as serviceWorker from './serviceWorker';
import initialData from "./initial-data";
import styled from "styled-components";
import Column from "./Column";
import Todo from "./Todo";

const PageContainer = styled.div`
    display:flex;
`;

const Calendar = styled.div`
    margin: 8px;
    border: 1px solid lightgrey;
    border-radius: 2px;
    width: 100%;
    display:flex;
    flex-direction: column;
    
`;

const Backlog = styled.div`
    margin: 8px;
    border: 1px solid lightgrey;
    border-radius: 2px;
    width: 20%;
    display:flex;
    flex-direction: column;
    
`;

const DailySchedule = styled.div`
    margin: 8px;
    border: 1px solid lightgrey;
    border-radius: 2px;
    width: 20%;
    display:flex;
    flex-direction: column;
    
`;

const Title = styled.h3`
    padding: 8px;
    text-align: center;
`;

class App extends React.Component {
    state = initialData;

    onDragEnd = result => {
        const { destination, source, draggableId } = result;

        if (!destination){
            return;
        }

        if (
            destination.droppableId === source.droppableId &&
            destination.index === source.index
        ){
            return;
        }

        const start = this.state.columns[source.droppableId]
        const finish = this.state.columns[destination.droppableId]

        if (start === finish){
            const newtodosIds = Array.from(start.todoIds);
            newtodosIds.splice(source.index,1);
            newtodosIds.splice(destination.index,0,draggableId);

            const newColumn = {
                ...start,
                todoIds: newtodosIds,
            };

            const newState = {
                ...this.state,
                backlog: {
                    ...this.state.columns,
                    [newColumn.id]: newColumn
                },
            };
            this.setState(newState);
            return;
        }

        const starttodoIds = Array.from(start.todoIds);
        starttodoIds.splice(source.index,1)
        const newStart = {
            ...start,
            todoIds: starttodoIds,
        };

        const finishTaksIds = Array.from(finish.todoIds);
        finishTaksIds.splice(destination.index,0,draggableId)
        const newFinish = {
            ...finish,
            todoIds: finishTaksIds,
        };

        const newState = {
            ...this.state,
            columns: {
                ...this.state.columns,
                [newStart.id]: newStart,
                [newFinish.id]: newFinish,
            },
        };
        this.setState(newState);




    };

    render(){
        return (
            <DragDropContext onDragEnd={this.onDragEnd}>
                <PageContainer>
                    <Backlog>
                        <Column
                            key={this.state.columns['column-1'].id}
                            column={this.state.columns['column-1']}
                            todos={this.state.columns['column-1'].todoIds
                                .map(todoId => this.state.todos[todoId])}
                        />
                    </Backlog>
                    <DailySchedule>
                        <Title>
                            DailySchedule
                        </Title>
                        {this.state.dailyScheduleColumns.map((col) => (
                            <Column
                                key={this.state.columns[col].id}
                                column={this.state.columns[col]}
                                todos={this.state.columns[col].todoIds
                                    .map(todoId => this.state.todos[todoId])}
                            />
                        ))}
                    </DailySchedule>
                    <Calendar>
                        <Title>
                            Calendar
                        </Title>
                        <table>
                            <tr>
                            {this.state.weekdays.map(day => (
                                <th>{day}</th>
                            ))}
                            </tr>
                            {this.state.calendarColumns.map((col,index) => {

                                if (index % 7){
                                }
                                let result = <td>
                                    <Column
                                        key={this.state.columns[col].id}
                                        column={this.state.columns[col]}
                                        todos={this.state.columns[col].todoIds
                                            .map(todoId => this.state.todos[todoId])}
                                    />
                                </td>


                                return result


                            })}
                        </table>

                    </Calendar>


                </PageContainer>
            </DragDropContext>
        );
    }
}
ReactDOM.render(<App/>, document.getElementById('root')
);



// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
//serviceWorker.unregister();
